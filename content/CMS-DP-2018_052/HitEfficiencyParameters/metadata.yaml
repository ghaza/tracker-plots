title: 'Parameters of a linear fit performed on the hit efficiency of the Silicon Strip detectors measured as function of the pileup for data recorded during the LHC fill 6714 (2018)'
caption: 'The hit efficiency is computed from this selection:

$\bullet$ Use tracks from the iterative tracking passing the "highPurity" selection;

$\bullet$ To avoid inactive regions such as the bonding region for modules with two sensors, and to take account of any residual misalignment, trajectories passing near the edges of sensors or their readout electronics in the studied layer are excluded from consideration. The efficiency is determined from the fraction of traversing tracks with a hit anywhere in the non-excluded region of a traversed module within a range of 15 strips;

$\bullet$ If the trajectory starts or ends in a considered module, it is discarded for computing the efficiency of the module. Thus, measurement in the first layer, TIB layer 1, relies on pixel seeding and moreover no measurement is possible in the outermost layers, ie TOB layer 6 and TEC disk 9;

$\bullet$ Known bad modules are excluded from the measurement. Moreover, the few modules with low efficiency (upper limit on the efficiency lower than the average layer efficiency minus 10%) are not included the average efficiency to avoid biases. 

For double layers, mono and stereo sensors are used for the computation ; the average efficiency is shown.
Interpretation of the results:

$\bullet$ The hit efficiency scales linearly with both the instantaneous luminosity and the pileup.

$\bullet$ The hit efficiency, greater than 98% even at high pileup, depends on the layer.

$\bullet$ As the inefficiency mainly depends on the particle flux, inner layers are more sensitive than the outer ones.

$\bullet$ Moreover, the inefficiency depends on the sensitive volume under the strip and thus depends at first order on the sensor thickness and to a less extent, on the pitch.

Parameters of a linear fit performed on the hit efficiency of the Silicon Strip detectors measured as function of the pileup for data recorded during the LHC fill 6714 (2018). The efficiency intercept at 0 pileup (in red) and the efficiency vs pileup slope (in blue) are reported for each measurement layer of tracker. Due to the selection criteria, no measurement can be performed in the outer layers : TOB L6 and TEC D9.'
date: '2018-09-12'
tags:
- Run-2
- Strips
- pp
- Collisions
- 2018