title: 'Lorentz Angle Calibration with Millepede II'
caption: 'The charge drift in B-field affects the measured hit position: $\Delta x = tan (\theta^{shift}_{LA}) \cdot d/2$. 

To correct this effect most precisely: the Lorentz angle $\theta^{shift}_{LA}$ calibration was integrated in MILLEPEDE II-alignment procedure. 

B-on and B-off data are used simultaneously in alignment to disentangle calibration and alignment effects. Used 60 million tracks from full 2012 data: tracks of isolated muons from  $Z^{0}\rightarrow\mu\mu$ decays, cosmic ray muons (B-on and B-off), low $p_{T}$, B-off pp data from August 2012. The Lorentz angle in the pixel barrel was calibrated with the granularity of 24 spatial parameters (3 layers x 8 rings) and 65 periods in time.

The plots shown are:

$\bullet$ Evolution of the Lorentz Angle in the Barrel Pixel Layer 1. The 8 sets of points each represent modules in a single ring of layer 1. Rings 4 and 5 (1 and 8) are closest to (furthest from) the centre of BPIX. General trend is a reduction as a function of the integrated luminosity, more pronounced for the inner rings.

$\bullet$ Evolution of the Lorentz Angle in the Barrel Pixel Layer 2. General trend is a rapid increase followed by a slow decrease. Smaller difference between the rings except the general offset between z+ and z- side.

$\bullet$ Evolution of the Lorentz Angle in the Barrel Pixel Layer 3. General trend is a slow increase followed by a slow decrease. Again smaller difference between the rings except the general offset between z+ and z- side.

$\bullet$ Evolution of the Lorentz Angle in the Ring 4  for each layer. Smaller radiation at larger radii stretches the curves and shifts them to the right. Otherwise qualitatively the same.
'
date: '2013-05-15'
tags:
- Run-1
- Time Evolution
- pp
- Collisions
- Phase-0 Pixel
- 2012
- Tracker
- Alignment